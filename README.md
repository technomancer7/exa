
# Exa
Exa is a mini programming language, inspired by [Zacktronics]() [Exapunks]() language.
Designed to be small, simple, but efficient, with a lot of useful utilities built in, and also built to be highly flexible and extensible.

While it shares some keywords and structure, and obviously the name (because I have very little imagination), it's not trying to be an exact clone, although there may be support that at some point. Really it's just a fun project I decided to make, because I really love playing Exapunks and it inspired me to create something from it.

The core is written in TypeScript using Deno, which allows it to function as a live scriptable environment, or be compiled in to a self-contained binary script runner.
It can be used as a REPL system, inputting and reading commands and viewing responses live, or commands can be saved in a text file and be read by the engine.