import libexa, os

var
    exa = newExa()

if paramCount() == 0:
    exa.repl()
else:
    echo paramStr(1)
    exa.exaFile(paramStr(1))