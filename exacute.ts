import { Exa, prompt } from "./libexa.ts";
let e = new Exa();

if(Deno.args.length == 0){
    var line = "";
    while(true){
        line = await prompt(e.state+" > ");
        e.parse(line);
    }

}else{
    let path = Deno.args[0];
    console.log("Loading... "+path);
    e.loadFile(path);
    e.compile();
    e.run();
}