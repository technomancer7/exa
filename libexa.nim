import rdstdin, strutils, re, tables
type
    Exa = object
        state, body:string
        variables:Table[string, string]
        lineNum, stage:int
        lines:seq[string]
        labels:Table[string, int]
        debug:bool


proc newExa*():Exa =
    result.state = "#MAIN"
    result.debug = true

proc set_var*(e:var Exa, key, v:string) =
    e.variables[key] = v

proc get_var*(e:var Exa, key:string):string =
    if e.variables.hasKey(key):
        return e.variables[key]
    else:
        return "#undefined"

proc isString*(s:string):bool =
    return s.match(re"'(.)+'")

proc isNum*(s:string):bool =
    try:
        discard parseInt(s)
        return true
    except:
        return false

proc exaPrint*(e:var Exa, line:string) =
    if line.isString():
        echo line
    elif line.isNum():
        echo "NUM: "&line
    else:
        e.exaPrint "'"&e.get_var(line)&"'"

proc symbol(e:var Exa, s:string): string =
    case s:
    of "#ln":
        return e.lineNum.intToStr
    of "#stage":
        return e.stage.intToStr
    else:
        return "#sym"

proc parseExaLine*(e:var Exa, s:string) =
    let cmd = s.split(" ")[0]
    let line = s.split(" ")[1..^1].join(" ")
    if s == "":
        return

    if e.stage == 0:
        if cmd notin @["mark", "defn", "echo", "halt", "copy"]:
            return

    case cmd:
    of "echo":
        echo "PRINT: "&line
        e.exaPrint(line)

    of "defn": #setting var only in compile stage
        let key = line.split("=")[0].strip()
        let v = line.split("=")[1].strip()
        e.variables[key] = v
        echo key&"="&e.variables[key]

    of "set": #setting var only in live stage
        if e.stage > 0:
            let key = line.split("=")[0].strip()
            let v = line.split("=")[1].strip()
            e.variables[key] = v
            if e.debug:
                echo key&"="&e.variables[key]

    of "copy":
        let c = e.variables[line.split(" ")[0]]
        e.set_var(line.split(" ")[1], c)
        

    of "mark":
        if e.lineNum >= 0:
            e.labels[line] = e.lineNum
        else:
            echo "#illegal_operation__marking_repl"

    of "*vars":
        echo e.variables

    of "*pre":
        e.stage = 0

    
    of "halt":
        quit(0)

    else:
        if s.startsWith("#"):
            echo e.symbol(s)
        else:
            echo "#ERROR ("&s&")"

proc compile*(e:var Exa, text:string) =
    e.lineNum = 0
    e.stage = 0
    e.body = text
    e.lines = text.split("\n")

    for line in e.lines:
        e.parseExaLine(line)
        e.lineNum = e.lineNum + 1


proc repl*(e:var Exa) =
    e.stage = 1
    e.lines = @[]
    var cl = ""
    while true:
        cl = readLineFromStdin(e.state&" > ")
        e.lines.add cl
        e.parseExaLine(cl)

proc exaFile*(e:var Exa, path:string)=
    let txt = readFile(path)
    e.compile(txt)
    e.lineNum = 0
    e.stage = 1
    if e.debug:
        echo "compile done"
    for line in e.lines:
        e.parseExaLine(line)
        e.lineNum = e.lineNum + 1
