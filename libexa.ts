import { IHash, Exa_Base } from "./objects.ts";

import {Exa_IO} from './libs/io.ts';
import {Exa_net} from './libs/net.ts';
import {Exa_sys} from './libs/sys.ts';
import {Exa_math} from './libs/math.ts';
/*
TODO

a help command
usable only in repl
`help <c>`
if c not given, lists all commands
finds command at c, and takes text from their `usage` and `help` fields and shows what `modes` they can 
be used in

`inspect <c>`
prints code of function for command c
only in repl

*/
export async function prompt(message: string = "") {
  const buf = new Uint8Array(1024);
  await Deno.stdout.write(new TextEncoder().encode(message));
  const n = <number>await Deno.stdin.read(buf);
  return new TextDecoder().decode(buf.subarray(0, n)).trim();
}


/*
--allow-net
--allow-read
*/

export class Exa extends Exa_Base {
    constructor() {
      super();
      Exa_IO.load_IO(this);
      Exa_net.load_net(this);
      Exa_sys.load_sys(this);
      Exa_math.load_math(this);
      
      this.commands["state"] = {
        execute: (e: Exa_Base, s:string) => { e.state = "#"+s.toUpperCase(); },
        usage: "state <s>",
        help: "Changes the runtime state descriptor."
      };

      this.commands["help"] = {
        execute: (e: Exa_Base, s:string) => {
          if(s == ""){
            e.print("COMMANDS");
            e.print("---")
            Object.keys(this.commands).forEach((key) => {
              e.print("<blue>"+key+"</blue>");
            });
          }else{
            let c = e.commands[s.toLowerCase()];
            if(c != undefined){
              if(c.usage != undefined){ e.print(e.commands[s].usage); }else{ e.print(s.toLowerCase()); }

              if(c.help != undefined){ e.print(c.help) }else{ e.print("No help defined for this command.") }
              if(c.modes != undefined && c.modes.length > 0) e.print("Usable in "+c.modes.join(", "))
            }else{ e.print("No command found for "+s.toLowerCase()); }
            
          }
        },
        modes: ["repl"], usage: "help [<c>]",
        help: "Shows help for a command, or shows list of command if <c> is not given."
      } 

      this.commands["note"] = {
        execute: (e: Exa_Base, s:string) => {},
        help: "A comment command, does nothing. Can be used to keep comments in code."
      } 

      this.commands["-"] = { execute: (e: Exa_Base, s:string) => {}, help: "Alias for `note`." } 
      this.commands["//"] = { execute: (e: Exa_Base, s:string) => {}, help: "Alias for `note`." } 
    }  
  }

