import {Exa_Base, FileRegister} from "../objects.ts";

export class Exa_IO {
    static load_IO(exa:Exa_Base){
        exa.commands["grab"] = {
            execute: (e: Exa_Base, s:string) => {
              let r = s.split(" ")[0].trim();
              let p = s.split(" ")[1].trim();
              e.grabFile(r, p);
            },
            modes: ["runtime", "repl"],
            help: "Opens file at path <path> and stores it in file register <register_name>.",
            usage: "grab (register_name) (path)"
        } 
        exa.commands["make"] = {
          execute: (e: Exa_Base, s:string) => {
            e.fileRegisters[s] = new FileRegister(s);
          },
          modes: ["runtime", "repl"],
          help: "Creates new file register from file at path <f>.", usage: "make (f)"
        } 
        exa.commands["commit"] = {
          execute: (e: Exa_Base, s:string) => { 
            if(e.fileRegisters[s] != undefined){ e.fileRegisters[s].save(); }
          }, modes: ["runtime", "repl"],
          help: "Saves <f> file register.", usage: "commit (f)"
        } 
        exa.commands["wipe"] = {
          execute: (e: Exa_Base, s:string) => {
            if(e.fileRegisters[s] != undefined){
              const p = Deno.run({ cmd: ["rm", e.fileRegisters[s].path] });
            }
          },
          modes: ["runtime", "repl"],
          help: "Deletes <f> file register. Only deletes the hard drive file, does not <drop> the file register, so it is possible to <commit> the file register and re-create the file.", usage: "wipe (f)"
        } 
        exa.commands["drop"] = {
          execute: (e: Exa_Base, s:string) => {
            e.dropFile(s);
          },
          modes: ["runtime", "repl"],
          help: "Removes <f> file register.", usage: "drop (f)"
        } 
        
        exa.commands["copy"] = {
          execute: (e: Exa_Base, s:string) => {
            let v = s.split(" ").slice(1).join(" ");
            let writev = e.contentsOf(v);

            let f = s.split(" ")[0];

            if(f.startsWith("file:")){
              if(e.fileRegisters[f.slice(5)]){
                e.fileRegisters[f.slice(5)].write(writev);
              }else{
                e.error("No file register at "+f);
              }
            }else{
              e.set_var(f, writev);
            }
          },
          modes: ["runtime", "repl"],
          help: "Copied data from value <v> in to container <f>.\nIf <v> is an Exa datatype, write that raw.\nIf <v> is a file register address (file:<id>), contents of that file will be used.\nIf <v> is a variable name, the value of that variable will be used.\nIf <f> is a file register address, value is written to the file. (Atomic, Overwrite)\nElse, <f> will be used as a variable name to store the value to.", usage: "copy f v"
        } 

        exa.commands["write"] = {
          execute: (e: Exa_Base, s:string) => {
            let fh = s.split(" ")[0];
            let sv = s.split(" ").slice(1).join(" ")
            let writev = e.contentsOf(sv);

            if(fh.startsWith("file:")){
              let handle = fh.split("file:")[1];
              if(e.fileRegisters[handle] != undefined){ e.fileRegisters[handle].write(writev) }
              else{ e.error("#error__invalid_file_handle ("+handle+")") }
            }else{
              e.set_var(fh, writev);
            }
          },
          modes: ["runtime", "repl"],
          usage: "write (fh) (sv)",
          help: "Writes contents of data sv to file handle or variable fh."
        } 
  

        exa.commands["append"] = {
          execute: (e: Exa_Base, s:string) => {
            let fh = s.split(" ")[0];
            let sv = s.split(" ").slice(1).join(" ")
            let writev = e.contentsOf(sv);
            let cur = e.get_var(fh);

            if(fh.startsWith("file:")){
              let handle = fh.split("file:")[1];
              if(e.fileRegisters[handle] != undefined){
                e.fileRegisters[handle].append(writev);
              }else{e.error("#error__invalid_file_handle ("+handle+")")}
            }else{
              e.set_var(fh, `${cur} ${writev}`);
            }
          },
          modes: ["runtime", "repl"],
          usage: "appendf (v) (sv)",
          help: "Appends contents of variable sv to variable or file v."
        } 
    
        exa.commands["prepend"] = {
          execute: (e: Exa_Base, s:string) => {
            let fh = s.split(" ")[0];
            let sv = s.split(" ").slice(1).join(" ")
            let writev = e.contentsOf(sv);
            let cur = e.get_var(fh);
            if(fh.startsWith("file:")){
              let handle = fh.split("file:")[1];
              if(e.fileRegisters[handle] != undefined){
                e.fileRegisters[handle].prepend(writev);
              }else{e.error("#error__invalid_file_handle ("+handle+")")}
            }else{
              e.set_var(fh, `${writev} ${cur}`);
            }
          },
          modes: ["runtime", "repl"],
          usage: "prependf (v) (sv)",
          help: "Prepends contents of variable sv to variable v."
        } 
    }
}