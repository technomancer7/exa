import { Exa_Base } from "../objects.ts";

export class Exa_math {
    static load_math(exa:Exa_Base){
        exa.commands["addi"] = {
            execute: (e: Exa_Base, s:string) => {
                let ov = s.split(" ")[0].trim(); //original value, can be num or variable
                let mod = s.split(" ")[1].trim(); //value to modify original value, can be num or variable
                let svt = s.split(" ")[2].trim(); //addr to save modified value to
                var base = 0;
                var tomod = 0;
                if(e.isNum(ov)){ base = Number(ov); } else{ base = Number(e.get_var(ov, "0")); }
                if(e.isNum(mod)){ tomod = Number(mod); } else{ tomod = Number(e.get_var(mod, "0")); }
                let newv = String(base+tomod);

                e.set_var(svt, newv)
            },
            modes: ["runtime", "repl"]
        } 
        exa.commands["subi"] = {
            execute: (e: Exa_Base, s:string) => {
                let ov = s.split(" ")[0].trim(); //original value, can be num or variable
                let mod = s.split(" ")[1].trim(); //value to modify original value, can be num or variable
                let svt = s.split(" ")[2].trim(); //addr to save modified value to
                var base = 0;
                var tomod = 0;
                if(e.isNum(ov)){ base = Number(ov); } else{ base = Number(e.get_var(ov, "0")); }
                if(e.isNum(mod)){ tomod = Number(mod); } else{ tomod = Number(e.get_var(mod, "0")); }
                e.set_var(svt, String(base-tomod))
            },
            modes: ["runtime", "repl"]
        } 
        exa.commands["muli"] = {
            execute: (e: Exa_Base, s:string) => {
                let ov = s.split(" ")[0].trim(); //original value, can be num or variable
                let mod = s.split(" ")[1].trim(); //value to modify original value, can be num or variable
                let svt = s.split(" ")[2].trim(); //addr to save modified value to
                var base = 0;
                var tomod = 0;
                if(e.isNum(ov)){ base = Number(ov); } else{ base = Number(e.get_var(ov, "0")); }
                if(e.isNum(mod)){ tomod = Number(mod); } else{ tomod = Number(e.get_var(mod, "1")); }
                e.set_var(svt, String(base*tomod))
            },
            modes: ["runtime", "repl"]
        } 
        exa.commands["divi"] = {
            execute: (e: Exa_Base, s:string) => {
                let ov = s.split(" ")[0].trim(); //original value, can be num or variable
                let mod = s.split(" ")[1].trim(); //value to modify original value, can be num or variable
                let svt = s.split(" ")[2].trim(); //addr to save modified value to
                var base = 0;
                var tomod = 0;
                if(e.isNum(ov)){ base = Number(ov); } else{ base = Number(e.get_var(ov, "0")); }
                if(e.isNum(mod)){ tomod = Number(mod); } else{ tomod = Number(e.get_var(mod, "1")); }
                e.set_var(svt, String(Math.round(base/tomod)))
            },
            modes: ["runtime", "repl"]
        } 
    }
}