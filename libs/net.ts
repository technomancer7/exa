import {Exa_Base } from "../objects.ts";

export class Exa_net {
    static load_net(exa:Exa_Base){
        exa.commands["fetch"] = {
            execute: (e: Exa_Base, s:string) => {
                // fetch url v
                // stores the data retrieved from web page at url in to variable v
                let vk = s.split(" ")[1].trim();
                let url = s.split(" ")[0].trim();
                const json = fetch(url);
    
                json.then((response) => {
                  response.text().then((c)=> e.set_var(vk, c))
                });
            },
            modes: ["runtime", "repl"]
          } 
    }
}