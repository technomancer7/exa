import { Exa_Base } from "../objects.ts";

export class Exa_sys {
    static load_sys(exa:Exa_Base){
 
        exa.commands["test"] = {
            execute: (e: Exa_Base, s:string) => {
                // define some globals for eval to use
                let tv = s.split(" ")[0];
                let eq = s.split(" ").slice(1).join(" ");
                
                let l = eq.split(" ")[0];
                let r = eq.split(" ")[2];
                let m = eq.split(" ")[1];
                var outcome = false;

                if(m == "=" && e.contentsOf(l) == e.contentsOf(r)) outcome = true;
                if(m == ">=" && e.contentsOf(l) >= e.contentsOf(r)) outcome = true;
                if(m == "<=" && e.contentsOf(l) <= e.contentsOf(r)) outcome = true;
                if(m == ">" && e.contentsOf(l) > e.contentsOf(r)) outcome = true;
                if(m == "<" && e.contentsOf(l) < e.contentsOf(r)) outcome = true;
                if(m == "!=" && e.contentsOf(l) != e.contentsOf(r)) outcome = true;
                //console.log(`${e.contentsOf(l)} ${m} ${e.contentsOf(r)}`)
                //c/onsole.log(outcome)
                if( outcome ){
                    e.set_var(tv, "1");
                }else e.set_var(tv, "0");

            },
            modes: ["runtime", "repl"], usage: "test (v) (e)", 
            help: "Tests evaluation e and saves wether it succeeds or fails to variable v."
        }
        exa.commands["etest"] = {
            execute: (e: Exa_Base, s:string) => {
                // define some globals for eval to use
                let tv = s.split(" ")[0];
                let eq = s.split(" ").slice(1).join(" ");
                
                if( eval(eq) ){
                    e.set_var(tv, "1");
                }else e.set_var(tv, "0");

            },
            modes: ["runtime", "repl"], usage: "etest (v) (e)", 
            help: "Tests JS evaluation e and saves wether it succeeds or fails to variable v."
        }
        exa.commands["testing"] = {
            execute: (e: Exa_Base, s:string) => {
                // define some globals for eval to use
                e.set_var("#testing", s);
                e.set_var("#"+s, "");
            },
            modes: ["*"], usage: "testing (v)", help: "Defines the variable used for test.."
        }

        exa.commands["jump"] = {
            execute: (e: Exa_Base, s:string) => {
                // define some globals for eval to use
                var nl = e.labelRegister[s];
                if(nl != undefined){
                    e.lineNum = nl;
                }
            },
            modes: ["runtime"], usage: "jump <label>", help: "Jumps to line at marked labelRegister."
        }

        exa.commands["tjmp"] = {
            execute: (e: Exa_Base, s:string) => {;
                var tv = e.get_var(s.split(" ")[0]);
                var nl = e.labelRegister[s.split(" ")[1]];

                if(nl != undefined && e.boolify(tv)){
                    e.lineNum = nl;
                }
            },
            modes: ["runtime"], usage: "tjmp (v) (label)", help: "Jumps to label if variable at v is truth."
        }
        exa.commands["fjmp"] = {
            execute: (e: Exa_Base, s:string) => {
                var tv = e.get_var(s.split(" ")[0])
                var nl = e.labelRegister[s.split(" ")[1]];

                if(nl != undefined && !e.boolify(tv)){
                    e.lineNum = nl;
                }
            },
            modes: ["runtime"], usage: "fjmp (v) (label)", help: "Jumps to label if variable at v is false."
        }
        exa.commands["eval"] = {
            execute: (e: Exa_Base, s:string) => {
                // define some globals for eval to use
                var v = e.variables;
                var f = e.fileRegisters;
                var c = e.commands;
                var print = e.print;
                var echo = e.echo;
                eval(s);
            },
            modes: ["*"]
        }
        

        
          exa.commands["dyn"] = {
            execute: (e: Exa_Base, s:string) => {
                let txt = Deno.readTextFileSync(s);
                eval(txt);
            },
            modes: ["compile", "repl"]
          } 
    }

}