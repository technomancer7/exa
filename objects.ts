import * as ink from 'https://deno.land/x/ink@1.3/mod.ts'
//import {existsSync } from "https://deno.land/std/fs/mod.ts";

export interface IHash {
    [details: string] : any;
} 

export class FileRegister {
    body:string = "";
    path:string = "";
  
    write(s:string){
      this.body = s;
    }
  
    append(s:string, delim="\n"){
      this.body = `${this.body}${delim}${s}`;
    }

    prepend(s:string, delim="\n"){
        this.body = `${s}${delim}${this.body}`;
    }
    save(new_path:string = ""){
      if(new_path != "") this.path = new_path;
      Deno.writeTextFileSync(this.path, this.body);
    }
  
    read(new_path:string = ""){
      if(new_path != "") this.path = new_path;
      try{
        this.body = Deno.readTextFileSync(this.path).toLowerCase();
      } catch (err){}
      
    }
  
    constructor(path:string = "") {
      if(path != ""){
        this.path = path;
        this.read();
      }
    }
}

export class Exa_Base {
    lines:string[] = [];
    logf:string[] = [];
    body: string = "";
    state = "#MAIN";

    testing = "test";
    debug = true;
    
    stage: number = -1;
    lineNum = -1;

    variables: IHash = {};
    fileRegisters: IHash = {};
    commands: IHash = {};
    labelRegister: IHash = {};
    string_regex = /["`](.)+["`]/;

    errors = {
      "#no_file_handle": "No file handle for %s."

    }

    constructor(){
        this.commands["halt"] = {
            execute: (e: Exa_Base, s:string) => {Deno.exit(0)},
            modes: ["*"]
        }

        this.commands["echo"] = {
            execute: (e: Exa_Base, s:string) => {e.echo(s)},
            modes: ["*"]
        }

        this.commands["defn"] = {
            execute: (e: Exa_Base, s:string) => {
              if(s.includes("=")){
                let key = s.split("=")[0].trim();
                let v = s.split("=")[1].trim();
                this.set_var(key, v.replaceAll(/["`]/g, ""));
              }else{
                this.set_var(s, "");
              }
    
            },
            modes: ["compile"]
        }

        this.commands["set"] = {
            execute: (e: Exa_Base, s:string) => {
              let key = s.split("=")[0].trim();
              let v = s.split("=")[1].trim();
              if(e.isString(v)){
                e.set_var(key, v.replaceAll(/["`]/g, ""));
              }else{
                e.error("#error__invalid_set_value")
              }
            },
            modes: ["runtime", "repl"]
          }
    
          this.commands["mark"] = {
            execute: (e: Exa_Base, s:string) => {
              //console.log(`Registering linenum ${this.lineNum-1}`)
              e.labelRegister[s] = this.lineNum-1;
              //console.log(e.labelRegister);
            },
            modes: ["compile"]
          } 
    
    }

    log(s:string){
      this.logf.push(s);
    }
    
    contentsOf(v:string){
      if(this.isNum(v)||this.isString(v)) return v.replaceAll(/["`]/g, "");
      if(this.isBool(v)) return String(this.boolify(v));

      if(v.startsWith("file:")){
        if(this.fileRegisters[v.slice(5)]){
          return this.fileRegisters[v.slice(5)].body;
        }else{this.error("No file register at "+v)}
      }else{
        return this.get_var(v);
      }
      
    }
    grabFile(register:string, path:string){
      this.fileRegisters[register] = new FileRegister(path);
    }

    dropFile(register:string){
      delete this.fileRegisters[register];
    }

    echo(s:string){
      if(this.isString(s)){
        this.writeln("[<blue>"+String(this.lineNum+1)+"</blue>]>> <yellow>"+s+"</yellow>");
      }else if(this.isNum(s)){
        this.writeln("[<blue>"+String(this.lineNum+1)+"</blue>] >> <green>"+s+"</green>");
      }else{
        if(this.get_var(s) != "")
          this.writeln(`[<blue>${this.lineNum+1}</blue>]?> ${this.get_var(s)}`);
        else
          this.writeln(`[<blue>${this.lineNum+1}</blue>]?> <yellow>${s}</yellow>`);
      } 
    }

    print(s:string){
      this.writeln("[<blue>"+String(this.lineNum+1)+"</blue>]>> <yellow>"+s+"</yellow>");
    }

    writeln(s:string){
      console.log(ink.colorize(s));
    }

    boolify(s:string){
      //console.log(`Boolifying ${s}`)
      if(this.isBool(s)){
        //console.log("is bool")
        if(["true", "t", "y", "1"].includes(s)){
          return true;
        }else if(["false", "f", "n", "0"].includes(s)){
          return false;
        } else return false;
        
      }else return false
    }
    isBool(s:string){
      return ["true", "false", "t", "f", "y", "n", "1", "0"].includes(s);
    }

    isString(s:string){
      return this.string_regex.test(s)
    }

    isNum(s:string){
      return !isNaN(Number(s));
    }

    ifVType(s:string){
      return (this.isNum(s)||this.isString(s)||this.isBool(s))
    }
    set_var(key:string, v:string){
      this.variables[key] = v;
    }

    get_var(key:string, v:string = ""){
      if(this.variables[key] == undefined)
        return v
      else return this.variables[key]
    }

    error(ln:string){
      if(this.debug) this.writeln("[<red>"+String(this.lineNum)+"</red>]!!>> <red>"+ln+"</red>")
    }

    err(code:string){
      /*if(this.errors[code] != undefined){
        this.error()
      }*/
    }

    parse(ln:string){
      if(ln == "") return;
      let parts = ln.split(" ");
      let command = parts[0];
      let line = parts.slice(1).join(" ");
      let ce = this.commands[command];

      if(ce == undefined){
        this.error("#error__invalid_command_symbol ("+command+")");
        return;
      }

      if(this.stage == -1){ //eval mode
        if(ce.modes == undefined){
          ce.execute(this, line);
        }else{
          if(ce.modes.includes("repl") || ce.modes.includes("*")){
            ce.execute(this, line);
          }else{
            this.error("#error__ilegal_operation ("+ln+" | MODE != repl|*)");
          }
        }

      }

      if(this.stage == 0){ //compile-time
        if(ce.modes == undefined){
          ce.execute(this, line);
        }else{
          if(ce.modes.includes("compile") || ce.modes.includes("*")){
            ce.execute(this, line);
          }else{
            this.error("#error__ilegal_operation ("+ln+" | MODE != compile|*)");
          }
        }
      }

      if(this.stage == 1){ //runtime
        if(ce.modes == undefined){
          ce.execute(this, line);
        }else{
          if(ce.modes.includes("runtime") || ce.modes.includes("*")){
            ce.execute(this, line);
          }else{
            this.error("#error__ilegal_operation ("+ln+" | MODE != runtime|*)");
          }
        }
      }
    }

    loadFile(path:string){
      this.body = Deno.readTextFileSync(path).toLowerCase();
      this.lines = this.body.split("\n");
    }

    compile(){
      this.lineNum = 1;
      this.stage = 0;
      for(let line of this.lines){
        this.parse(line);
        this.lineNum += 1;
      }
    }

    run(){
      this.echo("RUN");
      this.lineNum = 1;
      this.stage = 1;

      while(this.lineNum < this.lines.length){
        this.parse(this.lines[this.lineNum]);
        this.lineNum += 1;
      }
      /*for(let line of this.lines){
        this.parse(line);
        this.lineNum += 1;
      }*/
    }

}